# :coding: utf-8
# :copyright: Copyright (c) 2015 ftrack

import os
import shutil
import logging

import flask
from werkzeug import secure_filename

logger = logging.getLogger(__file__)

app = flask.Flask('ftrack-upload-example')
UPLOAD_FOLDER = None


@app.route('/put', methods=['PUT'])
def put():
    '''Upload single file.'''

    # The file has already been uploaded by nginx. Move the file to
    # specified location.
    temporary_filepath = flask.request.headers['X-File']
    file_id = flask.request.args.get('id')

    filepath = os.path.join(UPLOAD_FOLDER, file_id)

    shutil.move(temporary_filepath, filepath)

    return '', 200


@app.route('/get', methods=['GET'])
def get():
    '''Return file by *id*.'''
    file_id = flask.request.args.get('id')
    file_id = secure_filename(file_id)

    filepath = os.path.join(UPLOAD_FOLDER, file_id)
    response = flask.Response('')

    # Set header to tell nginx what file to serve.
    response.headers['X-Accel-Redirect'] = filepath

    return response


@app.route('/remove', methods=['GET', 'POST'])
def remove():
    '''Remove file by *id*.'''
    file_id = flask.request.args.get('id') or flask.request.form.get('id')
    file_id = secure_filename(file_id)

    filepath = os.path.join(UPLOAD_FOLDER, file_id)
    os.remove(filepath)

    return '', 200


def run(host, port, directory):
    '''Run server with *host*, *port* and *directory*.'''
    global UPLOAD_FOLDER
    UPLOAD_FOLDER = directory

    app.run(host=host, port=port, debug=True)
